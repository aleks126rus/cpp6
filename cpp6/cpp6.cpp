﻿#include <iostream>

class Vector
{
public:
	void Show()
	{  
		//массив1 - 3 значения
		for (i = 0; i < 3; i++) std::cin >> a[i];

		//массив2 - 3 значения
		for (i = 0; i < 3; i++) std::cin >> b[i];

		//Вычисление
		for (i = 0; i < 3; i++) c[i] = a[(i + 1) % 3] * b[(i + 2) % 3] - a[(i + 2) % 3] * b[(i + 1) % 3];

		//Результат
		std::cout << "[a.b] =";
		for (i = 0; i < 3; i++)
			std::cout << " " << c[i];
		std::cout << std::endl;	
	}
private:
	//Переменные и массивы
	int i;
	double a[3];
	double b[3];
	double c[3];
};

int main() 
{
		Vector v;
		v.Show();
		return 0;
	}



	